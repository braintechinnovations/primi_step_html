// function salutami(var_nome){
//     console.log("CIAO " + var_nome);
// }

$(document).ready(
    function (){

        // $("#button_inserimento").click(
        //     function (){
        //         // salutami("Giovanni");
        //         //console.log("CIAO");

        //         $("#input_titolo").val("GIOVANNI");

        //         //let titolo = $("#input_titolo").val();
        //         //console.log(titolo);
        //     }
        // );

        var array_libri = [];

        $("#button_svuota").on("click",
            function(){
                $("#contenuto_tabella").empty();
            }
        );

        $("#button_inserimento").on("click", 
            function(){
                let titolo  = $("#input_titolo").val();
                let autore  = $("#input_autore").val();
                let isbn    = $("#input_isbn").val();
                let tipo    = $("#select_tipologia").val();

                let libro_temp = {
                    tit: titolo,
                    aut: autore,
                    isb: isbn,
                    tip: tipo
                }

                array_libri.push(libro_temp);

                scansioneArrayLibri(array_libri);
            }
        );
    }
);

function scansioneArrayLibri(arr_lib){
    $("#contenuto_tabella").empty();

    for(let i=0; i<arr_lib.length; i++){
        aggiuntiLibroInTabella(arr_lib[i]);
    }
}

function aggiuntiLibroInTabella(obj_libro){
    let stringa_tabella =   "<tr>" +
                                "<td>" + obj_libro.tit + "</td>" + 
                                "<td>" + obj_libro.aut + "</td>" + 
                                "<td>" + obj_libro.isb + "</td>" + 
                                "<td>" + obj_libro.tip + "</td>" + 
                            "</tr>";

    $("#contenuto_tabella").append(stringa_tabella);
}

/*
Creare un Form di inserimento studenti che tramite JQuery aggiunga ogni nuovo studente all'interno di una tabella HTML.
Ogni studente sarà caratterizzato da:
- Nome
- Cognome
- Matricola
- Sesso

Ad ogni nuovo inserimento controllare se lo studente esiste prima di effettuare l'operazione.

--------------------
Creare un campo di ricerca per Nome che filtri i dati della tabella.
Il processo è:
1 Inseriserisco il nome nell'input form
2 Clicco sul tasto di ricerca
3 Rimangono in tabella solo i dati corrispondenti al criterio di ricerca
*/