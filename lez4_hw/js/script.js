$(document).ready(
    function(){

        scansioneArrayStudenti(array_studenti);             //Viene invocata appena ho dichiarato la variabile;

        $("#input_nome").on("focusout",
            function(){
                if($(this).val() != ""){
                    $("#input_cognome").removeAttr("disabled");
                    $("#input_matricola").show();
                }
            }
        );

        $("#inserisci_studente").on("click",
            function(){
                let var_nome = $("#input_nome").val();
                let var_cogn = $("#input_cognome").val();
                let var_matr = $("#input_matricola").val();
                let var_sess = $("#select_sesso").val();

                if(var_nome == "" || var_cogn == "" || var_matr == ""){
                    alert("Errore!");
                    return;
                }

                if(verificaEsistenza(array_studenti, var_matr)){
                    alert("Matricola già esistente!");
                    return;
                }

                let studente_temp = {
                    nome: var_nome,
                    cognome: var_cogn,
                    matricola: var_matr,
                    sesso: var_sess,
                }

                array_studenti.push(studente_temp);
                scansioneArrayStudenti(array_studenti);                        //Aggiorno la tabella con il contenuto dell'array
                alert("Studente inserito con successo!");

                $("#input_nome").val("");
                $("#input_cognome").val("");
                $("#input_matricola").val("");
                //TODO: Pulisci i valori della select!!! Seleziona il primo valore!!!
            }
        );

        $("#ricerca_studente").on("click",
            function(){
                let chiave_ricerca = $("#input_ricerca").val();
                ricercaInArray(array_studenti, chiave_ricerca);
            }
        );

        $("#ricerca_pulisci").on("click",
            function(){
                scansioneArrayStudenti(array_studenti); 
            }
        );

        // $(".pulsante-elimina").on("click",                           //NON FUNGE
        //     function(){
        //         debugger;
        //         let matricola = $(this).data("identificativo");
        //         //$(this).css("color", "blue");
        //         //console.log($(this));
        //         eliminaInArray(array_studenti, matricola);
        //     }
        // );
    }
);

//Variabile dove salvo tutti gli studenti
var array_studenti = [
    {
        matricola: "123456",
        nome: "Giovanni",
        cognome: "Pace",
        sesso: "M"
    },
    {
        matricola: "123457",
        nome: "Massimo",
        cognome: "Lince",
        sesso: "M"
    },
    {
        matricola: "123458",
        nome: "Maria",
        cognome: "Marchionne",
        sesso: "F"
    },
    {
        matricola: "123459",
        nome: "Giovanni",
        cognome: "Pastrocchio",
        sesso: "A"
    },
];

function eliminaInArray(var_array, var_matricola){
    for(let i=0; i<var_array.length; i++){
        if(var_array[i].matricola == var_matricola){
            var_array.splice(i, 1);                 //Rimuove 1 oggetto alla posizione i
        }
    }

    scansioneArrayStudenti(var_array);
}

function ricercaInArray(var_array, var_chiave){
    let array_ausiliario = [];

    for(let i=0; i<var_array.length; i++){
        if(var_array[i].nome == var_chiave || var_array[i].cognome == var_chiave){
            array_ausiliario.push(var_array[i]);
        }
    }

    scansioneArrayStudenti(array_ausiliario);
}

function verificaEsistenza(var_array, var_matricola){
    for(let i=0; i<var_array.length; i++){
        if(var_array[i].matricola == var_matricola){
            return true;
        }
    }

    return false;
}

function scansioneArrayStudenti(var_array){
    $("#lista_studenti").empty();

    for(let i=0; i<var_array.length; i++){
        aggiungiATabella(var_array[i]);
    }
}

function aggiungiATabella(obj_studente){

    let riga_studente = "<tr>" + 
                            "<td>" + obj_studente.matricola + "</td>" + 
                            "<td>" + obj_studente.nome + "</td>" + 
                            "<td>" + obj_studente.cognome + "</td>" + 
                            "<td>" + obj_studente.sesso + "</td>" +  
                            "<td><button type='button' class='btn btn-danger' onclick='eliminaStudente(this)' data-identificativo='" + obj_studente.matricola + "'>X</button></td>" + 
                        "</tr>";

    $("#lista_studenti").append(riga_studente);
}

function eliminaStudente(var_pulsante){
    let matricola = $(var_pulsante).data("identificativo");
    eliminaInArray(array_studenti, matricola);
}
