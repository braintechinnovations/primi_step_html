var array_studenti = [];

$(document).ready(
    function(){

        $("#tabella").hide();

        $("#inserisci_studente").click(
            function(){
                let var_nome = $("#input_nome").val();
                let var_cognome = $("#input_cognome").val();
                let var_data = $("#input_data").val();      //Stringa

                let pers_temp = {
                    nome: var_nome,
                    cognome: var_cognome,
                    data_nas: var_data
                }

                if(verificaMaggiorenne(var_data)){
                    array_studenti.push(pers_temp);
                    stampaTabella();
                    $("#tabella").show();
                }
                else{
                    alert("Attenzione, non sei maggiorenne!");
                }

            }
        );

        $("#svuota_tabella").click(
            function(){
                array_studenti.empty();
                stampaTabella();
                $("#tabella").hide();
            }
        );

    }
);

//Return true se maggiorenne, false altrimenti!
function verificaMaggiorenne(var_data_nascita){
    debugger; 
    let parsa_data = new Date(var_data_nascita);

    let oggi = new Date();

    let diciotto_anni_fa = oggi.getFullYear() - 18;
    oggi.setHours(0);
    oggi.setMinutes(0);
    oggi.setSeconds(0);
    oggi.setFullYear(diciotto_anni_fa)

    let diciotto_anni_fa_da_oggi = oggi.getTime();

    let data_nascita_millisecondi = Date.parse(var_data_nascita);
    data_nascita = new Date(data_nascita_millisecondi);
    data_nascita.setHours(0);
    data_nascita.setMinutes(0);
    data_nascita.setSeconds(0);
    
    if(data_nascita_millisecondi <= diciotto_anni_fa_da_oggi){
        return true;
    }

    return true;
}

function stampaTabella(){
    $("#lista_studenti").empty();

    for(let i=0; i<array_studenti.length; i++){
        let riga_tabella = "<tr><td>" + array_studenti[i].nome + "</td><td>" + array_studenti[i].cognome + "</td><td>" + array_studenti[i].data_nas + "</td></td>"
        $("#lista_studenti").append(riga_tabella);
    }
}