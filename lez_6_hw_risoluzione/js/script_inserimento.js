$(document).ready(
    function(){

        let elenco_string = localStorage.getItem("elenco_studenti");
        
        // var elenco_studenti = [];
        // if(elenco_string != null){
        //     elenco_studenti = JSON.parse(elenco_string);
        // }

        //Operatore ternario: condizione ? espressione1 : espressione2 
        var elenco_studenti = elenco_string != null ? JSON.parse(elenco_string) : [];

        $("#pulisci_form").click(
            function(){
                pulisci_campi();
            }
        );

        $("#inserisci_studente").click(
            function(){
                let var_nome        = $("#input_nome").val();
                let var_cognome     = $("#input_cognome").val();
                let var_matricola   = $("#input_matricola").val();
                let var_nascita     = $("#input_nascita").val();

                // if(var_nome == "" || var_cognome == "" || var_matricola == "" || var_nascita == ""){
                //     alert("Attenzione, i campi obbligatori non sono completi!");
                //     return;
                // }

                if(!verificaInputSemplice(var_nome, "Nome", "input_nome"))
                    return;

                if(!verificaInputSemplice(var_cognome, "Cognome", "input_cognome"))
                    return;

                if(!verificaInputSemplice(var_matricola, "Matricola", "input_matricola"))
                    return;

                if(!verificaInputSemplice(var_nascita, "Data di nascita", "input_nascita"))
                    return;

                let stud_temp = {
                    nome: var_nome,
                    cognome: var_cognome,
                    matricola: var_matricola,
                    nascita: var_nascita,
                }

                elenco_studenti.push(stud_temp);

                let elenco_json = JSON.stringify(elenco_studenti);
                localStorage.setItem("elenco_studenti", elenco_json);

                alert("Studente inserito con successo");
                pulisci_campi();
            }
        );

    }
);

function pulisci_campi(){
    $("#input_nome").val("");
    $("#input_cognome").val("");
    $("#input_matricola").val("");
    $("#input_nascita").val("");
    // $("#input_sesso").prop('selectedIndex',  0); //Set del valore di default della select
    // $("#input_sesso").val("F").change(); //Set del valore di default della select tramite changer
}

/**
 * Funzione per la verifica dell'input
 * @param {String} val_campo Valore del campo da verificare
 * @param {String} str_nome Nome da inserire nell'alert
 * @param {String} foc_input ID (senza #) dell'input su cui fare il focus
 * @returns TRUE se il campo è valido, FALSE altrimenti.
 */
function verificaInputSemplice(val_campo, str_nome, foc_input){
    if(val_campo.trim() == ""){
        alert("Attenzione, manca il campo " + str_nome + "!");
        $("#" + foc_input).focus();
        return false;
    }

    return true;
}