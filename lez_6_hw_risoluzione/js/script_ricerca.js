var studenti_json;

$(document).ready(
    function(){

        var studenti_stringa = localStorage.getItem("elenco_studenti");
        studenti_json = studenti_stringa != null ? JSON.parse(studenti_stringa) : [];

        stampaTabella();

        // $("#apri-modale").click(
        //     function(){
        //         $("#modale-modifica").modal("show");
        //     }
        // );

        $("#cta_update_studente").click(
            function(){    
                let val_nome = $("#modal_nome").val();
                let val_cognome = $("#modal_cognome").val();
                let val_nascita = $("#modal_nascita").val();

                let val_matricola =  $("#modale-modifica").data("matricoladaagg");

                for(let i=0; i<studenti_json.length; i++){
                    if(studenti_json[i].matricola == val_matricola){
                        studenti_json[i].nome = val_nome;
                        studenti_json[i].cognome = val_cognome;
                        studenti_json[i].nascita = val_nascita;
                    }
                }

                let elenco_json = JSON.stringify(studenti_json);
                localStorage.setItem("elenco_studenti", elenco_json);
                window.location.reload();
            }
        );

    }
);

function stampaTabella(){

    let tab = $("#contenuto_tabella");
    tab.empty();

    for(let i=0; i<studenti_json.length; i++){
        let str_riga = "<tr data-matricola=\"" + studenti_json[i].matricola + "\">" +
                "<td>" + studenti_json[i].nome +  "</td>" + 
                "<td>" + studenti_json[i].cognome +  "</td>" + 
                "<td>" + studenti_json[i].matricola +  "</td>" + 
                "<td class=\"text-center\">" + 
                    "<button type=\"button\" class=\"btn btn-danger\" onclick=\"eliminaStudente(this)\"><i class=\"fa fa-trash\"></i>" + 
                    "<button type=\"button\" class=\"btn btn-warning\" onclick=\"modificaStudente(this)\"><i class=\"fa fa-pen\"></i>" + 
                "</td>" + 
            "</tr>";

        tab.append(str_riga);
    }

}

function eliminaStudente(obj_pulsante){
    let pulsante = $(obj_pulsante);
    let var_matricola = pulsante.parent().parent().data("matricola");       //TODO: Andatevi a vedere i Child, Find, Parent

    for(let i=0; i<studenti_json.length; i++){
        if(studenti_json[i].matricola == var_matricola){
            studenti_json.splice(i, 1);
        }
    }

    let string_elenco = JSON.stringify(studenti_json);
    localStorage.setItem("elenco_studenti", string_elenco);

    stampaTabella();
}

function modificaStudente(obj_pulsante){
    let pulsante = $(obj_pulsante);
    let var_matricola = pulsante.parent().parent().data("matricola");

    for(let i=0; i<studenti_json.length; i++){
        if(studenti_json[i].matricola == var_matricola){
            $("#modal_nome").val(studenti_json[i].nome);
            $("#modal_cognome").val(studenti_json[i].cognome);
            $("#modal_nascita").val(studenti_json[i].nascita);
        }
    }

    $("#matricola_studente").text(var_matricola);
    $("#modale-modifica").data("matricoladaagg", var_matricola);
    $("#modale-modifica").modal("show");
}